package at.wolff.observables;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Lantern l1 = new Lantern();
		ChristmasLight cl1 = new ChristmasLight();
		TrafficLight tl1 = new TrafficLight();
		Sensor s1 = new Sensor();
		
		s1.addObservable(l1);
		s1.addObservable(cl1);
		s1.addObservable(tl1);
		s1.addObservable(l1);

		s1.informAll();

	}

}
