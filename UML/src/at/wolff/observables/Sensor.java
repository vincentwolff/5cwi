package at.wolff.observables;
import java.util.ArrayList;
import java.util.List;

public class Sensor {
	
	private List<Observable> observables;
	
	public Sensor() {
		this.observables = new ArrayList<Observable>();
	}
	
	public void addObservable(Observable os) {
		observables.add(os);
	}
	
	public void informAll(){
	    for (Observable observable : observables) {
	    	
	      observable.inform();
	    }
	}
}
