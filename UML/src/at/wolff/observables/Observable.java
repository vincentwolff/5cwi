package at.wolff.observables;

public interface Observable {

	public void inform();
	
}
