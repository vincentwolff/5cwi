package at.wolff.playable;

public class Song extends CD implements Playable{

	private String name;

	
	public Song(String name) {
		super(name);
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void play() {
		System.out.println("Ich heiße "+ this.getName() + " und bin ein Song.");		
		
	}


	
}
