package at.wolff.playable;

import java.util.ArrayList;
import java.util.List;

public class Player {
	
	public List<Playable> playables;

	
	public Player() {
		super();
		this.playables = new ArrayList<Playable>();
	}

	public void addPlayable(Playable playables) {
		this.playables.add(playables);
	}
	
	public void playAll() {
		for (Playable playable : playables) {
			playable.play();
		}
	}
	
}
