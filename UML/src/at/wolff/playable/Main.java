package at.wolff.playable;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Player player1 = new Player();
		CD cd1 = new CD("1");
		DVD dvd1 = new DVD("1");
		Title title1 = new Title("Oceans Eleven");
		Title title2 = new Title("Titanic");
		Song s1 = new Song("Africa");
		
		
		dvd1.addTitle(title1); 
		dvd1.addTitle(title2);
		cd1.addTitle(s1);
		
		player1.addPlayable(title1);
		player1.addPlayable(title2);
		player1.addPlayable(s1);
		player1.playAll();
	}

}
