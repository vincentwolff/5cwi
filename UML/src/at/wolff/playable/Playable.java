package at.wolff.playable;

public interface Playable {
	public void play();
}
