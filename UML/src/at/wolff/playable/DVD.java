package at.wolff.playable;

import java.util.ArrayList;
import java.util.List;

public class DVD {

	private String title;
	public List<Title> titles;
	
	public DVD (String title) {
		super();
		this.title = title;
		this.titles = new ArrayList<Title>();
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void addTitle(Title title) {
		this.titles.add(title);
	}
}
