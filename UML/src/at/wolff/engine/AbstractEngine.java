package at.wolff.engine;

public abstract class AbstractEngine implements Engine {
	private int serial;

	public AbstractEngine(int serial) {
		super();
		this.serial = serial;
	}

	public int getSerial() {
		return serial;
	}

	public void setSerial(int serial) {
		this.serial = serial;
	}
	
	
}
