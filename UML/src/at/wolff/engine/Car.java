package at.wolff.engine;

public class Car {

	private Engine engine;
	
	public Car(Engine engine) {
		super();
		this.engine = engine;
	}
	
	public void gas(int amountOfGas) {
		int amount = amountOfGas/2;
		this.engine.run(amount);
	}
	
	public void getSerialnumber() {
		System.out.print("Meine Seriennummer ist " + this.engine.getSerialnumber());
	}
	
}
