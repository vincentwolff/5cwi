package at.wolff.encrypter;

public class Cesar implements Encrypter {

	public int shift;

	public Cesar() {
		super();
		this.shift = 7;
	}

	public Cesar(int shift) {
		super();
		this.shift = shift;

	}

	@Override
	public String encript(String content) {
		// TODO Auto-generated method stub

		String result = "";
		
		//char[] eArray = content.toCharArray();
		
		

		for (int i = 0; i < content.length(); i++) {
			char character = content.charAt(i);
			int ascii = (int) character;

			char shifted = (char) (ascii + shift);
			
			if (shifted > 122) {
				
				int dif = shifted - 122;
				
				shifted = (char) (dif + 96);
				
			}


			result = result + shifted;

		}

		return result;
	}

	@Override
	public String decript(String content) {
		// TODO Auto-generated method stub

		String result = "";

		for (int i = 0; i < content.length(); i++) {
			char character = content.charAt(i);
			int ascii = (int) character;
			char shifted = (char) (ascii - shift);
			
			if (shifted < 97) {
				
				int dif = 97 - shifted;
				
				shifted = (char) (123 - dif);
				
			}

			result = result + shifted;

		}
		return result;

	}
}
