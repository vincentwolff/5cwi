package at.wolff.tictactoe;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TicTacToe ttt = new TicTacToe();
		boolean player1turn = true;
		
		
		while(true) {
			
			ttt.showBoard();
			String[] selectedField = ttt.input();
			ttt.fillInBoard(selectedField, player1turn);
			ttt.checkWin();
			player1turn = !player1turn;
			
		}
		
	}

}
