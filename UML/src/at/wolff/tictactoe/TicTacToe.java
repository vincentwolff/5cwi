package at.wolff.tictactoe;

import java.util.Scanner;

public class TicTacToe {

	int[][] board = new int[3][3];
	Scanner s = new Scanner(System.in);

	public void showBoard() {

		System.out.println(board[0][0] + "|" + board[0][1] + "|" + board[0][2]);
		System.out.println(board[1][0] + "|" + board[1][1] + "|" + board[1][2]);
		System.out.println(board[2][0] + "|" + board[2][1] + "|" + board[2][2]);

	}

	public String[] input() {

		String input = s.next();
		String[] inputString = input.split(",");

		return inputString;

	}

	public void fillInBoard(String[] selectedField, boolean player1turn) {

		int column = 0;
		int row = 0;
		try {
			column = Integer.parseInt(selectedField[0]);
			row = Integer.parseInt(selectedField[1]);
		} catch (NumberFormatException e) {

			System.out.println("Falsche Eingabe. Richtig: z.B: 0,1");

		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Falsche Eingabe. Richtig: z.B: 0,1");
		}

		if (player1turn == true) {board[row][column] = 1;} 
		else {board[row][column] = 2;}
	}

	public void checkWin() {
		
		boolean didWin = false;
		
		if(     board[0][0] == board[1][0] && board[1][0] == board[2][0] && board[0][0] > 0) {didWin = true;}
		else if(board[0][1] == board[1][1] && board[1][1] == board[2][1] && board[0][1] > 0) {didWin = true;}
		else if(board[0][2] == board[1][2] && board[1][2] == board[2][2] && board[0][2] > 0) {didWin = true;}
		
		else if(board[0][0] == board[0][1] && board[0][1] == board[0][2] && board[0][0] > 0) {didWin = true;}
		else if(board[1][0] == board[1][1] && board[1][1] == board[1][2] && board[1][0] > 0) {didWin = true;}
		else if(board[2][0] == board[2][1] && board[2][1] == board[2][2] && board[2][0] > 0) {didWin = true;}

		else if(board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] > 0) {didWin = true;}
		else if(board[2][0] == board[1][1] && board[1][1] == board[0][2] && board[2][0] > 0) {didWin = true;}
		
		if (didWin == true) {
			System.out.println("Gewonnen");
		}
	}
}
