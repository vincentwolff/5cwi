package at.wolff.factorypattern;

public class Homer implements Actor {
	private int id;

	public Homer(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public void move() {
		// TODO Auto-generated method stub
		System.out.println("Homer bewegt sich.");
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		System.out.println("Homer rendert");
		
	}
	
}