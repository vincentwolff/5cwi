package at.wolff.factorypattern;

public class Snowflake implements Actor {
	private int id;

	public Snowflake(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public void move() {
		// TODO Auto-generated method stub
		System.out.println("Snowflake bewegt sich.");
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		System.out.println("Snowflake rendert");
	}
	
}