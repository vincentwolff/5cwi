package at.wolff.factorypattern;

public interface Actor {
	
	
	public void move();
	
	public void render();
}