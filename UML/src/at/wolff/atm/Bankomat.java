package at.wolff.atm;

import java.util.Scanner;

public class Bankomat {

	public static void main(String args[]) {
		int balance = 5000, abheben, aufladen;
		Scanner s = new Scanner(System.in);
		while (true) {
			System.out.println("Automated Teller Machine");
			System.out.println("Wähle 1 zum Abheben");
			System.out.println("Wähle 2 zum Aufladen");
			System.out.println("Wähle 3 um den Kontostand zu prüfen");
			System.out.println("Wähle 4 um abzubrechen");
			int n = s.nextInt();
			switch (n) {
			case 1:
				System.out.print("Wie viel möchtest du abhebben?:");
				abheben = s.nextInt();
				if (balance >= abheben) {
					balance = balance - abheben;
					
				} else {
					System.out.println("Insufficient Balance");
				}
				System.out.println("");
				break;

			case 2:
				System.out.print("Wie viel Geld möchtest du Einzahlen?");
				aufladen = s.nextInt();
				balance = balance + aufladen;
				break;

			case 3:
				System.out.println("Kontostand : " + balance);
				break;

			case 4:
				System.exit(0);
			}
		}
	}
}
