package at.wolff.viergewinnt;

import java.util.Scanner;

public class vierGewinnt {

	int[][] board = new int[6][7];
	Scanner s = new Scanner(System.in);
	int column = 5;
	int row = 0;

	public void showBoard() {

		System.out.println(board[0][0] + "|" + board[0][1] + "|" + board[0][2] + "|" + board[0][3] + "|" + board[0][4] + "|" + board[0][5] + "|" + board[0][6]);
		System.out.println(board[1][0] + "|" + board[1][1] + "|" + board[1][2] + "|" + board[1][3] + "|" + board[1][4] + "|" + board[1][5] + "|" + board[1][6]);
		System.out.println(board[2][0] + "|" + board[2][1] + "|" + board[2][2] + "|" + board[2][3] + "|" + board[2][4] + "|" + board[2][5] + "|" + board[2][6]);
		System.out.println(board[3][0] + "|" + board[3][1] + "|" + board[3][2] + "|" + board[3][3] + "|" + board[3][4] + "|" + board[3][5] + "|" + board[3][6]);
		System.out.println(board[4][0] + "|" + board[4][1] + "|" + board[4][2] + "|" + board[4][3] + "|" + board[4][6] + "|" + board[4][6] + "|" + board[4][6]);
		System.out.println(board[5][0] + "|" + board[5][1] + "|" + board[5][2] + "|" + board[5][3] + "|" + board[5][4] + "|" + board[5][5] + "|" + board[5][6]);

	}

	public String input() {

		String input = s.next();
		return input;

	}

	public void fillInBoard(String selectedField, boolean player1turn) {



		try {
			row = Integer.parseInt(selectedField);
		} catch (NumberFormatException e) {

			System.out.println("Falsche Eingabe. Richtig: z.B: 0,1");

		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Falsche Eingabe. Richtig: z.B: 0,1");
		}
		
		while (board[column][row] == 1) {
			column = column - 1;
		}

		board[column][row] = 1;
		
		column = 5;
		
	//	if (player1turn == true) {board[row][column] = 1;} 
	//	else {board[row][column] = 2;}
	}

	public void checkWin() {
		
		boolean didWin = false;
		
		if(     board[0][0] == board[1][0] && board[1][0] == board[2][0] && board[0][0] > 0) {didWin = true;}
		else if(board[0][1] == board[1][1] && board[1][1] == board[2][1] && board[0][1] > 0) {didWin = true;}
		else if(board[0][2] == board[1][2] && board[1][2] == board[2][2] && board[0][2] > 0) {didWin = true;}
		
		else if(board[0][0] == board[0][1] && board[0][1] == board[0][2] && board[0][0] > 0) {didWin = true;}
		else if(board[1][0] == board[1][1] && board[1][1] == board[1][2] && board[1][0] > 0) {didWin = true;}
		else if(board[2][0] == board[2][1] && board[2][1] == board[2][2] && board[2][0] > 0) {didWin = true;}

		else if(board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] > 0) {didWin = true;}
		else if(board[2][0] == board[1][1] && board[1][1] == board[0][2] && board[2][0] > 0) {didWin = true;}
		
		if (didWin == true) {
			System.out.println("Gewonnen");
		}
	}
}
