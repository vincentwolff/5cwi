package at.wolff.fertilizer;

public interface FertilizeStrategy {
	public void doFertilize();
}
