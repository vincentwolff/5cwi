package at.wolff.fertilizer;

public class Main {

	public static void main(String[] args) {
		
		Area area1 = new Area(1000);
		
		
		Tree tree1 = new LeafTree(1,new TopGrow());
		Tree tree2 = new Connifer(1,new SuperGrow());
		Tree tree3 = new LeafTree(1,new TopGrow());
		

		area1.addTree(tree1);
		area1.addTree(tree2);
		area1.addTree(tree3);
		
		
		area1.Fertilize();
	}

}
