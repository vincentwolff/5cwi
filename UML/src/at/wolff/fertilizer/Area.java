package at.wolff.fertilizer;

import java.util.ArrayList;
import java.util.List;

public class Area {
	private int size;
	public List<Tree> trees;
	
	public Area(int size) {
		super();
		this.size = size;
		this.trees = new ArrayList<Tree>();
	}
	
	public void addTree(Tree trees) {
		this.trees.add(trees);
	}
	
	public void Fertilize() {
		for (Tree tree : trees) {
			tree.getStrategy().doFertilize();;
		}	
	}
	
	
}
